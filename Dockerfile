FROM node:12-alpine

WORKDIR /opt/recipe

COPY package*.json ./
RUN npm ci

COPY . .

EXPOSE 3000

CMD ["node", "src/server.js"]
