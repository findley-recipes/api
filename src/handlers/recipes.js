let restify = require('restify');
var errors  = require('restify-errors');
let Recipe  = require('../models').Recipe;
let DRecipe = require('../models').DRecipe;
let utils   = require('../util/utils.js');
let meta    = require('../util/meta.js');

exports.create = function(req, res, next) {
    Recipe.create(req.body, function(err, r) {
        if (err) return res.send(utils.parseError(err));
        res.send(r);

        let ai = utils.processIngData(r.ingredients);
        meta.addMetadata(r.tags, r.category, ai.names, ai.units);
    });
};

exports.update = function(req, res, next) {
    // DB call handler
    let h = function(err, r) {
        if (err) return res.send(utils.parseError(err));
        if (r == null) return res.send(new errors.NotFoundError('No recipe with this id exists'));
        res.send(r);

        // Update ingredient, tag, and category metadata
        let ai = utils.processIngData(r.ingredients);
        meta.addMetadata(req.body.tags, req.body.category, ai.names, ai.units);
    };

    if (utils.isObjectId(req.params.id)) {
        req.body.dateModified = Date.now();
        Recipe.findByIdAndUpdate(req.params.id, {$set: req.body}, {runValidators: true},  h);
    } else {
        Recipe.update({linkName: req.params.id.toLowerCase()}, {$set: req.body}, {runValidators: true}, h);
    }
};

exports.delete = function(req, res, next) {
    var h = function(err, r) {
        if (err) return res.send(parseError(err));
        if (r == null) return res.send(new errors.NotFoundError('No recipe with this id exists'));

        let deletedRecipe = DRecipe.create(r, (err, rr) => {
            if (err) return res.send(utils.parseError(err));
            return res.send({success: true});
        });

        r.remove();
    };

    if (utils.isObjectId(req.params.id)) {
        Recipe.findById(req.params.id, h);
    } else {
        Recipe.findOne({linkName: req.params.id.toLowerCase()}, h);
    }
};

// Look up a recipe using either the linkName or the _id
exports.get = function(req, res, next) {
    var h = function(err, r) {
        if (err) return res.send(utils.parseError(err));
        if (r == null) return res.send(new errors.NotFoundError('No recipe with this id exists'));
        return res.send(r);
    };

    if (utils.isObjectId(req.params.id)) {
        Recipe.findById(req.params.id, h);
    } else {
        Recipe.findOne({linkName: req.params.id.toLowerCase()}, h);
    }
};

exports.search = function(req, res, next) {
    // Sanitize query variables
    if (req.query.limit && req.query.limit > 50) req.query.limit = 50;
    if (req.query.limit == null) req.query.limit = '25';
    req.query.limit = parseInt(req.query.limit);
    if (req.query.limit < 0) req.query.limit = -req.query.limit;
    if (req.query.dir == null ) req.query.dir = 'asc';
    if (req.query.dir != 'asc' && req.query.dir != 'desc') req.query.dir = 'asc';
    if (req.query.page == null) req.query.page = '1';
    req.query.page = parseInt(req.query.page);
    if (req.query.page < 1) req.query.page = 1;
    if (req.query.orderby == null) req.query.orderby = 'name';
    if (req.query.tags) {
        req.query.tags = req.query.tags.split(',');
        req.query.tags = req.query.tags.map(function(e) { return e.toLowerCase(); });
    }

    var sort = {};
    sort[req.query.orderby] = req.query.dir;

    var filter = {};
    if (req.query.category) filter.category = req.query.category;
    if (req.query.q) {
        filter.$text = {$search: req.query.q};
        sort = {score: {$meta: 'textScore'}};
        req.query.orderby = 'score';
    }
    if (req.query.tags) filter.tags = {$in: req.query.tags};

    var q = Recipe.find(filter);
    q.skip( (req.query.page-1)*req.query.limit )
    .limit(req.query.limit)
    .sort(sort)
    .select(utils.listProjection)
    .exec()
    .then(function(r) {
        return [r, Recipe.find(filter).count()];
    })
    .spread(function(r, c) {
        res.setHeader('X-Result-Count', c);
        res.setHeader('X-Result-Limit', req.query.limit);
        res.setHeader('X-Result-Page', req.query.page);
        res.setHeader('X-Result-Dir', req.query.dir);
        res.setHeader('X-Result-Orderby', req.query.orderby);
        if (req.query.q) res.setHeader('X-Result-Q', req.query.q);
        return res.send(r);
    })
    .catch(function(err) {
        return res.send(utils.parseError(err));
    });
};

exports.typeahead = function(req, res, next) {
    if (!req.params.pre) return res.send([]);
    var reg = new RegExp('.*'+req.params.pre + '.*', 'gi');
    Recipe.find({name: reg})
    .limit(8)
    .sort({printCount: 'desc'})
    .select({name: 1})
    .exec()
    .then(function(names) {
        res.send(names);
    })
    .catch(function(err) {
        return res.send(utils.parseError(err));
    });
};

exports.print = function(req, res, next) {
    var h = function(err, r) {
        if (err) {
            console.log(err);
            res.send(500, {success: false});
        } else {
            res.send({success: true});
        }
    };
    if (utils.isObjectId(req.params.id)) {
        Recipe.findByIdAndUpdate(req.params.id, {$inc: {printCount: 1}}, h);
    } else {
        Recipe.update({linkName: req.params.id.toLowerCase()}, {$inc: {printCount: 1}}, h);
    }
};

exports.meta = function(req, res, next) {
    res.send(meta.data);
};
